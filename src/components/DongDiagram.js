import { fabric } from "fabric";
import { useEffect, useRef, useState } from "react";
import Arrow from "../entities/Arrow";

export function DongDiagram({ json }) {
    const canvasRef = useRef();
    const fontSize = 15;
    const rectWidth = 200;
    const rectheight = 50;
    let gapY = 100;
    let gap = 30;
    let gapX = 0;
    const fitImage = 10;
    const paddingLeft = 10;
    let offsetY = 100;
    let maxLeft = 0;

    useEffect(() => {
        const parent = document.getElementById("canvasContainer");
        const { clientWidth: canvasWidth, clientHeight: canvasHeight } = parent;
        canvasRef.current.width = canvasWidth;
        canvasRef.current.height = canvasHeight;

        var canvas = new fabric.Canvas("canvas");

        const rectOption = {
            width: rectWidth,
            height: rectheight,
            top: 0,
            left: 0,
            fill: "rgba(0,0,0,0)",
            stroke: "black",
            strokeWidth: 1,
        };

        const rect = new fabric.Rect(rectOption);
        const text = new fabric.Textbox(json.text, {
            width: rectWidth,
            textAlign: "center",
        });
        const group = new fabric.Group([rect, text], {
            left: canvasWidth / 2 - rect.width / 2,
        });
        canvas.add(group);

        const arrowoptions = {
            stroke: "black",
            arrowLength: 10,
            fill: "rgba(0,0,0,0)",
        };

        canvas.add(
            new Arrow(
                [
                    { x: canvasWidth / 2, y: rect.height },
                    { x: canvasWidth / 2, y: rect.height + 40 },
                ],
                {
                    ...arrowoptions,
                    left: canvasWidth / 2,
                    top: rect.height,
                }
            )
        );
        canvas.add(
            new Arrow(
                [
                    { x: canvasWidth / 2, y: rect.height },
                    { x: canvasWidth / 2, y: rect.height + 20 },
                    { x: canvasWidth / 2 - 250, y: rect.height + 20 },
                    { x: canvasWidth / 2 - 250, y: rect.height + 40 },
                ],
                {
                    ...arrowoptions,
                    left: canvasWidth / 2 - 250,
                    top: rect.height,
                }
            )
        );
        canvas.add(
            new Arrow(
                [
                    { x: canvasWidth / 2, y: rect.height },
                    { x: canvasWidth / 2, y: rect.height + 20 },
                    { x: canvasWidth / 2 + 400, y: rect.height + 20 },
                    { x: canvasWidth / 2 + 400, y: rect.height + 40 },
                ],
                {
                    ...arrowoptions,
                    left: canvasWidth / 2,
                    top: rect.height,
                }
            )
        );

        const offsetX =
            (canvasWidth -
                (rectWidth * json.children.length +
                    gap * json.children.length)) /
            2;

        for (let i = 0; i < json.children.length; i++) {
            const element = json.children[i];

            const rect = new fabric.Rect(rectOption);
            const text = new fabric.Text(element.text, {
                width: rectWidth - rectheight - paddingLeft,
                fontSize: fontSize,
                left: rectheight + paddingLeft,
                fixedWidth: rectWidth - rectheight - paddingLeft,
            });
            text.top = rectheight / 2 - text.fontSize / 2;
            if (text.width > text.fixedWidth - rectheight) {
                let newFontSize = fontSize * (text.fixedWidth / text.width);
                text.fontSize = newFontSize > 15 ? 15 : newFontSize;
                text.width = rectWidth - rectheight - paddingLeft;
            }
            const posX = offsetX + (rectWidth + gap) * i + gapX;
            console.log("pox", posX, offsetX);
            const group = new fabric.Group([rect, text], {
                left: posX,
                top: gapY,
                // width: rectWidth,
                // height: rectheight
            });
            fabric.Image.fromURL(require("../assets/user.jpg"), function (img) {
                let scale = (rectheight - fitImage) / img.height;
                img.scale(scale).set({
                    left: -rectWidth / 2 + fitImage / 2 + paddingLeft,
                    top: -rectheight / 2 + fitImage / 2,
                    clipPath: new fabric.Circle({
                        radius: 200,
                        originX: "center",
                        originY: "center",
                    }),
                });
                group.add(img);
                canvas.add(group);
            });
            if (element.children && element.children.length > 0) {
                offsetY = 100;
                // gapX = 0;
                drawRect(
                    canvas,
                    element.children,
                    posX + gap,
                    offsetY,
                    rectOption
                );
                // gap += 30;
            }
        }
    }, []);

    const drawRect = (canvas, listChild, startLeft, startTop, rectOption) => {
        for (let i = 0; i < listChild.length; i++) {
            const element = listChild[i];

            const rect = new fabric.Rect(rectOption);
            const text = new fabric.Text(element.text, {
                width: rectWidth - rectheight - paddingLeft,
                fontSize: fontSize,
                left: rectheight + paddingLeft,
                fixedWidth: rectWidth - rectheight - paddingLeft,
            });
            text.top = rectheight / 2 - text.fontSize / 2;
            if (text.width > text.fixedWidth - rectheight) {
                let newFontSize = fontSize * (text.fixedWidth / text.width);
                text.fontSize = newFontSize > 15 ? 15 : newFontSize;
                text.width = rectWidth - rectheight - paddingLeft;
            }
            offsetY = offsetY + rectheight + 10;
            const group = new fabric.Group([rect, text], {
                left: startLeft,
                top: offsetY,
            });
            fabric.Image.fromURL(require("../assets/user.jpg"), function (img) {
                let scale = (rectheight - fitImage) / img.height;
                img.scale(scale).set({
                    left: -rectWidth / 2 + fitImage / 2 + paddingLeft,
                    top: -rectheight / 2 + fitImage / 2,
                    clipPath: new fabric.Circle({
                        radius: 200,
                        originX: "center",
                        originY: "center",
                    }),
                });
                group.add(img);
                canvas.add(group);
            });
            // console.log('ve')
            if (startLeft > maxLeft) {
                maxLeft = startLeft;
                gapX += 30;
                // console.log('gap + 30', gapX)
            }
            if (element.children && element.children.length > 0) {
                drawRect(
                    canvas,
                    element.children,
                    startLeft + 30,
                    offsetY,
                    rectOption
                );
            }
        }
    };

    return (
        <div
            id="canvasContainer"
            style={{
                position: "fixed",
                top: "0px",
                left: "0px",
                width: "100%",
                height: "100%",
            }}
        >
            <canvas
                ref={canvasRef}
                id="canvas"
                width="800"
                height="700"
            ></canvas>
        </div>
    );
}
