import { fabric } from "fabric";
import { useEffect, useRef } from "react";
import TextBox from "../entities/TextBox";
import { uuidv4 } from "../utils/common";

export function TestCanvas() {
    const canvasRef = useRef();
    const textWidth = 100;

    useEffect(() => {
        const parent = document.getElementById("canvasContainer");
        const { clientWidth: canvasWidth, clientHeight: canvasHeight } = parent;
        canvasRef.current.width = canvasWidth;
        canvasRef.current.height = canvasHeight;

        var canvas = new fabric.Canvas("canvas");

        // fabric.Image.fromURL("/images/user.jpg", function (img) {

        // });

        const textBoxOption = {
            width: 100,
            textAlign: "left",
            top: 100,
            left: 100,
            icon: "/images/user.jpg",
            abc: "fsdf",
        };

        const id = uuidv4();
        const text = new TextBox(id, "Cong trinh dang thi cong", textBoxOption);
        canvas.add(text);

        window.text = text;
        window.canvas = canvas;

        //canvas.add(new fabric.IText("Cong trinh", { top: 100, left: 100 }));
    }, []);

    return (
        <div
            id="canvasContainer"
            style={{
                position: "fixed",
                top: "0px",
                left: "0px",
                width: "100%",
                height: "100%",
            }}
        >
            <canvas
                ref={canvasRef}
                id="canvas"
                width="800"
                height="700"
            ></canvas>
        </div>
    );
}
