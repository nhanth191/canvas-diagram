import "./App.css";
import { DongDiagram } from "./components/DongDiagram";
import { TestCanvas } from "./components/TestCanvas";

function App() {
    const json = {
        text: "Giám đốc",
        children: [
            {
                text: "Phòng ban 1",
                children: [
                    { text: "Bộ phận 1.1" },
                    { text: "Bộ phận 1.2" },
                    {
                        text: "Bộ phận 1.3",
                        children: [{ text: "Bộ phận 1.3.1" }],
                    },
                ],
            },
            {
                text: "Phòng ban 2",
                children: [
                    {
                        text: "Bộ phận 2.1",
                        children: [
                            {
                                text: "Nhóm/Tổ 2.1.1",
                                children: [
                                    {
                                        text: "Bộ phận 2.1.1.1",
                                        children: [
                                            {
                                                text: "Bộ phận 2.1.1.1.1",
                                                children: [
                                                    {
                                                        text: "Bộ phận 2.1.1.1.1.1",
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                    { text: "Bộ phận 2.1.1.2" },
                                    { text: "Bộ phận 2.1.1.3" },
                                ],
                            },
                            { text: "Nhóm/Tổ 2.1.2" },
                        ],
                    },
                    // {
                    //   text: "Bộ phận 2.2",
                    //   children: [
                    //     { text: "Nhóm/Tổ 2.2.1" },
                    //     { text: "Nhóm/Tổ 2.2.2" },
                    //     { text: "Nhóm/Tổ 2.2.3" }
                    //   ]
                    // }
                ],
            },
            {
                text: "Phòng ban 3",
                children: [{ text: "Nhóm/Tổ 3.1" }, { text: "Nhóm/Tổ 3.2" }],
            },
        ],
    };
    return (
        <div className="App">
            <TestCanvas />
        </div>
    );
}

export default App;
