import { fabric } from "fabric";
import { Vector } from "../utils/Vector";

const Arrow = fabric.util.createClass(fabric.Polyline, {
    type: "Arrow",
    initialize: function (points, options) {
        options || (options = {});

        this.callSuper("initialize", points, options);

        //custom params
        this.set("arrowWidth", options.arrowWidth || 5);
        this.set("arrowLength", options.arrowLength || 10);
    },

    toObject: function () {
        return fabric.util.object.extend(this.callSuper("toObject"), {
            arrowWidth: this.get("arrowWidth"),
            arrowLength: this.get("arrowLength"),
        });
    },

    _render: function (ctx) {
        this.callSuper("_render", ctx);

        if (this.points.length < 2) {
            return;
        }

        ctx.fillStyle = this.stroke;

        const box = this.points.reduce(
            (prev, { x, y }) => {
                prev.min.x = Math.min(prev.min.x, x);
                prev.max.x = Math.max(prev.max.x, x);

                prev.min.y = Math.min(prev.min.y, y);
                prev.max.y = Math.max(prev.max.y, y);

                return prev;
            },
            { min: { x: 9999, y: 9999 }, max: { x: -9999, y: -9999 } }
        );

        const v1 = new Vector(
            this.points[this.points.length - 2].x,
            this.points[this.points.length - 2].y,
            0
        );
        const v2 = new Vector(
            this.points[this.points.length - 1].x,
            this.points[this.points.length - 1].y,
            0
        );
        let dir = v2.subtract(v1).unit();
        const center = new Vector(
            (box.max.x + box.min.x) / 2,
            (box.max.y + box.min.y) / 2
        );
        const o = v2.subtract(center);
        const p3 = o.add(dir.multiply(this.arrowLength));
        dir = v2.subtract(v1).cross(new Vector(0, 0, 1)).unit();
        const p1 = o.add(dir.multiply(this.arrowWidth));
        dir = dir.multiply(-1);
        const p2 = o.add(dir.multiply(this.arrowWidth));

        ctx.beginPath();
        ctx.moveTo(p1.x, p1.y);
        ctx.lineTo(p2.x, p2.y);
        ctx.lineTo(p3.x, p3.y);
        ctx.closePath();

        ctx.fill();
    },
});

export default Arrow;
