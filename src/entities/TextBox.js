import { fabric } from "fabric";
import { uuidv4 } from "../utils/common";

var deleteImg = document.createElement("img");
deleteImg.src =
    "data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAiIGhlaWdodD0iMTIiIHZpZXdCb3g9IjAgMCAxMCAxMiIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTcuMzMzMjkgNC4yNVYxMC4wODMzSDIuNjY2NjNWNC4yNUg3LjMzMzI5Wk02LjQ1ODI5IDAuNzVIMy41NDE2M0wyLjk1ODI5IDEuMzMzMzNIMC45MTY2MjZWMi41SDkuMDgzMjlWMS4zMzMzM0g3LjA0MTYzTDYuNDU4MjkgMC43NVpNOC40OTk5NiAzLjA4MzMzSDEuNDk5OTZWMTAuMDgzM0MxLjQ5OTk2IDEwLjcyNSAyLjAyNDk2IDExLjI1IDIuNjY2NjMgMTEuMjVINy4zMzMyOUM3Ljk3NDk2IDExLjI1IDguNDk5OTYgMTAuNzI1IDguNDk5OTYgMTAuMDgzM1YzLjA4MzMzWiIgZmlsbD0iIzI3MjcyNyIgZmlsbC1vcGFjaXR5PSIwLjYiLz4KPC9zdmc+Cg==";

const addImg = document.createElement("img");
addImg.src =
    "data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTQiIGhlaWdodD0iMTQiIHZpZXdCb3g9IjAgMCAxNCAxNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHJlY3Qgd2lkdGg9IjE0IiBoZWlnaHQ9IjE0IiBmaWxsPSJ3aGl0ZSIvPgo8cGF0aCBkPSJNNy41MjUgNC4zNzVINi40NzVWNi40NzVINC4zNzVWNy41MjVINi40NzVWOS42MjVINy41MjVWNy41MjVIOS42MjVWNi40NzVINy41MjVWNC4zNzVaTTcgMS43NUM0LjEwMiAxLjc1IDEuNzUgNC4xMDIgMS43NSA3QzEuNzUgOS44OTggNC4xMDIgMTIuMjUgNyAxMi4yNUM5Ljg5OCAxMi4yNSAxMi4yNSA5Ljg5OCAxMi4yNSA3QzEyLjI1IDQuMTAyIDkuODk4IDEuNzUgNyAxLjc1Wk03IDExLjJDNC42ODQ3NSAxMS4yIDIuOCA5LjMxNTI1IDIuOCA3QzIuOCA0LjY4NDc1IDQuNjg0NzUgMi44IDcgMi44QzkuMzE1MjUgMi44IDExLjIgNC42ODQ3NSAxMS4yIDdDMTEuMiA5LjMxNTI1IDkuMzE1MjUgMTEuMiA3IDExLjJaIiBmaWxsPSIjMzJBMUM4Ii8+Cjwvc3ZnPgo=";

const paintImg = document.createElement("img");
paintImg.src =
    "data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTIiIGhlaWdodD0iMTIiIHZpZXdCb3g9IjAgMCAxMiAxMiIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTguNjYwMzYgNi4yNzE3NkwzLjQ0NjA4IDAuNzVMMi42MjM3IDEuNjIwODhMNC4wMTE4NCAzLjA5MDg4TDEuMDA4MDkgNi4yNzE3NkMwLjY2Mzk3IDYuNjM2MTggMC42NjM5NyA3LjIyMjk0IDEuMDA4MDkgNy41ODExOEw0LjIxNTk4IDEwLjk3ODJDNC4zODUxMiAxMS4xNTc0IDQuNjEyNTkgMTEuMjUgNC44MzQyMiAxMS4yNUM1LjA1NTg2IDExLjI1IDUuMjgzMzMgMTEuMTU3NCA1LjQ1MjQ3IDEwLjk3ODJMOC42NjAzNiA3LjU4MTE4QzkuMDA0NDggNy4yMjI5NCA5LjAwNDQ4IDYuNjM2MTggOC42NjAzNiA2LjI3MTc2Wk0yLjA0MDQ1IDYuOTI2NDdMNC44MzQyMiAzLjk2Nzk0TDcuNjI4IDYuOTI2NDdIMi4wNDA0NVpNMTAuMDgzNSA3Ljg1Mjk0QzEwLjA4MzUgNy44NTI5NCA4LjkxNjk5IDkuMTkzMjQgOC45MTY5OSAxMC4wMTQ3QzguOTE2OTkgMTAuNjk0MSA5LjQ0MTkyIDExLjI1IDEwLjA4MzUgMTEuMjVDMTAuNzI1MSAxMS4yNSAxMS4yNSAxMC42OTQxIDExLjI1IDEwLjAxNDdDMTEuMjUgOS4xOTMyNCAxMC4wODM1IDcuODUyOTQgMTAuMDgzNSA3Ljg1Mjk0WiIgZmlsbD0iIzI3MjcyNyIgZmlsbC1vcGFjaXR5PSIwLjYiLz4KPC9zdmc+Cg==";

const backgroundWidth = 95;
const backgroundHeight = 30;
const backgroundOffsetRight = -backgroundWidth - 30;

const TextBox = fabric.util.createClass(fabric.IText, {
    type: "TextBoxEx",
    initialize: function (id, text, options) {
        options || (options = {});
        this.callSuper("initialize", text, options);

        this.set("id", id || uuidv4());

        if (options.icon) {
            fabric.Image.fromURL(options.icon, (img) => {
                this.set("_icon", img.getElement());
                this._render(this._cacheContext);
                this.canvas.renderAll();
            });
        }
    },
    padding: 20,
    _render: function (ctx) {
        this.callSuper("_render", ctx);
        ctx.strokeRect(
            -this.width / 2 - 20,
            -this.height / 2 - 20,
            this.width + 40,
            this.height + 40
        );

        const iconWidth = 30;
        const iconHeight = 30;

        if (this._icon) {
            ctx.drawImage(
                this._icon,
                -this.width / 2,
                -iconHeight / 2,
                iconWidth,
                iconHeight
            );
        }
    },
    _getLeftOffset: function () {
        return this.direction === "ltr" ? -this.width / 2 + 50 : this.width / 2;
    },
    calcTextWidth: function () {
        var maxWidth = this.getLineWidth(0);

        for (var i = 1, len = this._textLines.length; i < len; i++) {
            var currentLineWidth = this.getLineWidth(i);
            if (currentLineWidth > maxWidth) {
                maxWidth = currentLineWidth;
            }
        }
        return maxWidth + 50;
    },
    controls: {
        background: new fabric.Control({
            x: 0.5,
            y: 0.5,
            offsetY: -10,
            offsetX: backgroundOffsetRight,
            cursorStyle: "pointer",
            render: function (ctx, left, top, styleOverride, fabricObject) {
                ctx.save();
                ctx.translate(left, top);
                ctx.rotate(fabric.util.degreesToRadians(fabricObject.angle));
                ctx.fillStyle = "white";
                ctx.fillRect(0, 0, backgroundWidth, backgroundHeight);
                ctx.strokeStyle = "gray";
                ctx.strokeRect(0, 0, backgroundWidth, backgroundHeight);
                ctx.restore();
            },
            cornerSize: 24,
        }),
        add: new fabric.Control({
            x: 0.5,
            y: 0.5,
            offsetY: 5,
            offsetX: backgroundOffsetRight + 20,
            cursorStyle: "pointer",
            mouseUpHandler: function (eventData, transform) {
                // add child
                var target = transform.target;
                console.log(target.id);
            },
            render: renderIcon(addImg),
            cornerSize: 20,
        }),
        bg: new fabric.Control({
            x: 0.5,
            y: 0.5,
            offsetY: 5,
            offsetX: backgroundOffsetRight + 45,
            cursorStyle: "pointer",
            mouseUpHandler: function (eventData, transform) {
                //change background
            },
            render: renderIcon(paintImg),
            cornerSize: 20,
        }),
        delete: new fabric.Control({
            x: 0.5,
            y: 0.5,
            offsetY: 5,
            offsetX: backgroundOffsetRight + 70,
            cursorStyle: "pointer",
            mouseUpHandler: function (eventData, transform) {
                // delete
                var target = transform.target;
                var canvas = target.canvas;
                canvas.remove(target);
                canvas.requestRenderAll();
            },
            render: renderIcon(deleteImg),
            cornerSize: 20,
        }),
    },
});

function renderIcon(icon) {
    return function renderIcon(ctx, left, top, styleOverride, fabricObject) {
        var size = this.cornerSize;
        ctx.save();
        ctx.translate(left, top);
        ctx.rotate(fabric.util.degreesToRadians(fabricObject.angle));
        ctx.drawImage(icon, -size / 2, -size / 2, size, size);
        ctx.restore();
    };
}

export default TextBox;
